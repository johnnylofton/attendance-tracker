class EventsController < ApplicationController
  http_basic_authenticate_with name: "admin", password: "admin", except: [:index]

  def index
    @events = Event.all
  end

  def show
    @event = Event.find(params[:id])
    @scans = @event.scans.all
    @scan = Scan.new

    respond_to do |format|
      format.html
      format.csv {send_data @scans.as_csv, filename: "#{@event.title}-#{Date.today}.csv" }
    end
  end

  def new
    @event = Event.new
  end

  def edit
    @event = Event.find(params[:id])
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      flash[:success] = "Successful event creation!"
      redirect_to @event
    else
      render 'new'
    end
  end

  def update
    @event = Event.find(params[:id])
    if @event.update(event_params)
      redirect_to @event
    else
      render 'edit'
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    redirect_to events_path
  end

  private
    def event_params
      params.require(:event).permit(:title, :start)
    end

end
