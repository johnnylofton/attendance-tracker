class StudentsController < ApplicationController
  http_basic_authenticate_with name: "admin", password: "admin", except: [:index, :show]

  def index
    @students = Student.all
  end

  def import
    Student.import(params[:file].path)
    flash[:success] = "Students Imported!"
  rescue NoMethodError
    flash[:danger] = "No file selected!"
  rescue ActiveRecord::RecordNotUnique
    flash[:danger] = "There was a Student ID conflict!"
    redirect_to students_path
  end

  def show
    @student = Student.find(params[:id])
    @scans = @student.scans.all
  end

  def new
    @student = Student.new
  end

  def edit
    @student = Student.find(params[:id])
  end

  def create
    @student = Student.new(student_params)
    if @student.save
      flash[:success] = "Successful student creation!"
      redirect_to @student
    else
      render 'new'
    end
  rescue ActiveRecord::RecordNotUnique => e
    flash[:danger] = "Student ID already exists!"
    redirect_to students_path
  end

  def update
    @student = Student.find(params[:id])
    if @student.update(student_params)
      redirect_to @student
    else
      render 'edit'
    end
  end

  def destroy
    @student = Student.find(params[:id])
    @student.destroy
    redirect_to students_path
  end

  private
    def student_params
      params.require(:student).permit(:first, :last, :year, :id)
    end

end
