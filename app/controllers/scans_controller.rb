class ScansController < ApplicationController

  def new
  end

  def edit
    @scan = Scan.find(params[:id])
  end

  # build scan into student and event ids
  def create
    # if barcode scanner input, marked by "?" at end of string,
    # this first line will select the ID part of the input
    if params[:scan][:student_id].last == "?"
      params[:scan][:student_id] = params[:scan][:student_id][7...15]
    end
    @event = Event.find(params[:scan][:event_id])
    @student = Student.find(params[:scan][:student_id])
    @scan = @event.scans.build(scan_params)
    if @scan.save
      flash[:success] = "Successful entry!"
      redirect_to @event
    else
      flash[:danger] = "Scan NOT successful!"
      redirect_to @event
    end
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = "Student NOT found!"
    redirect_to @event
  end

  def update
    @scan = Scan.find(params[:id])
    if @scan.update(scan_params)
      redirect_to @scan.event
    else
      render 'edit'
    end
  end

  def destroy
  end

  private
    def scan_params
      params.require(:scan).permit(:notes, :student_id, :event_id)
    end
end
