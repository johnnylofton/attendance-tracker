class Student < ApplicationRecord
  has_many :scans, dependent: :destroy
  has_many :events, through: :scans
  validates :first, presence: true
  validates :last,  presence: true
  validates :year,  presence: true
  validates :id,   presence: true

  #import passing file as arg
  def self.import(file)
    CSV.foreach(file, :headers => true) do |row|
      id, first, last, year = row
      Student.create!(row.to_hash)
    end
  end

  def full_name
    "#{last}, #{first} - Class of #{year}"
  end
end
