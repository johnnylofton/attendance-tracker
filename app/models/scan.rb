class Scan < ApplicationRecord
  belongs_to :event
  belongs_to :student
  validates :student_id, presence: true
  validates :event_id, presence: true

  def self.as_csv
    header = %w[id last first scan_time notes]
    CSV.generate do |csv|
      csv << header
      all.each do |scan|
        csv << [
          scan.student_id,
          scan.student.last,
          scan.student.first,
          scan.created_at,
          scan.notes
        ]
      end
    end
  end

end
