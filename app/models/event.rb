class Event < ApplicationRecord
  has_many :scans, dependent: :destroy
  has_many :students, through: :scans
  validates :title, presence: true
end
