# Ruby on Rails Attendance Scanner Application

Getting Started

Clone the repo and install needed gems:
```
$ bundle install
```
Next, migrate database:

```
$ rails db:migrate
```
Run the rails test suite
```
$ rails test
```
If there are no errors, the application is ready to be run on locally
```
$ rails server
```

Made with:
*  ruby 2.5.1
*  rails 5.2.0
*  sqlite3
