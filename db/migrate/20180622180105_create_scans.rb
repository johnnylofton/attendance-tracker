class CreateScans < ActiveRecord::Migration[5.2]
  def change
    create_table :scans do |t|
      t.text :notes
      t.references :event, foreign_key: true
      t.references :student, foreign_key: true

      t.timestamps
    end
    add_index :scans, [:student_id, :created_at]
  end
end
