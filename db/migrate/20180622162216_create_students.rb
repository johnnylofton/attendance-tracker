class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :first
      t.string :last
      t.integer :year

      t.timestamps
    end
  end
end
