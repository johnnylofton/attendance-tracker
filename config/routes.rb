Rails.application.routes.draw do
  root 'static_pages#home'

  resources :events
  resources :students do
    collection { post :import }
  end
  resources :scans

  get '/help', to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
end
