require 'test_helper'

class EventTest < ActiveSupport::TestCase
  def setup
    @event = Event.new(title: "Example", start: "07/04/2018 2:00PM")
  end

  test "should be valid" do
    assert @event.valid?
  end

  test "title should be present" do
    @event.title = "    "
    assert_not @event.valid?
  end
end
