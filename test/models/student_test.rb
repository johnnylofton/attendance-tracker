require 'test_helper'

class StudentTest < ActiveSupport::TestCase

  def setup
    @student = Student.new(first: "Example", last: "Student", year: "2020", id: "123456")
  end

  test "should be valid" do
    assert @student.valid?
  end

  test "first should be present" do
    @student.first = "    "
    assert_not @student.valid?
  end

  test "last should be present" do
    @student.last = "    "
    assert_not @student.valid?
  end

  test "year should be present" do
    @student.year = "    "
    assert_not @student.valid?
  end

  test "id should be present" do
    @student.id = "    "
    assert_not @student.valid?
  end
end
