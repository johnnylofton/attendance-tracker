require 'test_helper'

class ScanTest < ActiveSupport::TestCase

  def setup
    @scan = Scan.new(notes: "MyNotes", student_id: students(:joe).id,
                                       event_id:   events(:party).id)
  end

  test "should be valid" do
    assert @scan.valid?
  end

  test "user id should be present" do
    @scan.student_id = nil
    assert_not @scan.valid?
  end

  test "event id should be present" do
    @scan.event_id = nil
    assert_not @scan.valid?
  end

end
